window.addEventListener("DOMContentLoaded", async () => {
  const selectTag = document.getElementById("conference");

  const url = "http://localhost:8000/api/conferences/";
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();

    for (let conference of data.conferences) {
      const option = document.createElement("option");
      option.value = conference.href;
      option.innerHTML = conference.name;
      selectTag.appendChild(option);
    }
    selectTag.classList.remove("form-select","d-none");

    let div = document.querySelector("#loading-conference-spinner");
    div.classList.add("form-select", "d-none");
  }

  const formTag = document.getElementById('create-attendee-form');
  formTag.addEventListener('submit', async event => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData.entries()));
    console.log(json)

    const conferenceURL = 'http://localhost:8001/api/attendees/';
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(conferenceURL, fetchConfig);
    if (response.ok) {
      formTag.reset();
      const newAttendee = await response.json();
      const successAlert = document.getElementById('success-message');
      successAlert.classList.remove("form-select","d-none"); // remove d-none from success alert
      const formDiv = document.getElementById('create-attendee-form');
      formDiv.classList.add("form-select","d-none"); // add d-none to the form
    }
  });
});
