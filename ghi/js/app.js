window.addEventListener("DOMContentLoaded", async () => {
  function createCard(name, description, pictureUrl, date, location) {
    return `
        <div class="card">
          <img class="card-img-top" src="${pictureUrl}">
          <div class="card-body">
            <h5 class="card-title"> ${name}</h5>
            <h6 class="card-subtitle"> ${location}</h6>
            <p class="card-text">${description}</p>
            <div class="card-footer text-muted"> This Conference Starts On: ${date} </div>
          </div>
        </div>
  `;
  }

  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw "no good "; //not sure about this
    } else {
      const data = await response.json();

      let index=0;
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const nameTag = details.conference.name;
          const conferenceDescription = details.conference.description;
          const conferenceImage = details.conference.location.picture_url;
          const conferenceDate = details.conference.starts;
          const conferenceLocation = details.conference.location;
          const html = createCard(
            nameTag,
            conferenceDescription,
            conferenceImage,
            conferenceDate,
            conferenceLocation,
          );
          const column = document.querySelector(`#col-${index%3}`);
          column.innerHTML += html;
          index +=1;
        }
      }
    }
  } catch (e) {
    console.error(e); // or this
  }
});
