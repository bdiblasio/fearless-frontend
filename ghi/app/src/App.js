function App(props) {
  return (
    <div className="container">
      <table>
        <thead>
          <tr>
            {props.attendees.map((attendee) => {
              return (
                <tr key={attendee.href}>
                  <td>{attendee.name}</td>
                  <td>{attendee.conference}</td>
                </tr>
              );
            })}
          </tr>
        </thead>
        <tbody></tbody>
      </table>
    </div>
  );
}

export default App;
